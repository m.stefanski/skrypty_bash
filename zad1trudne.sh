#!/bin/bash

declare -A words
counter=1
chars=(",",".")

while read -r line
do
    arr=(${line// / })
    for i in "${arr[@]}"
    do
       :
       if [[ "${chars[@]}" =~ "${i: -1}" ]]
       then
        word="${i::-1}"
        words["${word,,}"]+=" ${counter}"
       else
        words["${i,,}"]+=" ${counter}"
       fi
    done
    counter=$((counter+1))
done < $1

for key in ${!words[@]}
do
    echo ${key} ${words[${key}]}
done | sort