Zadania łatwe (po 5 pkt.)
1. Napisz skrypt, który czyta podany plik tekstowy i wypisuje go na stdout, ale dodając dodatkowy pusty wiersz po każdym wierszu pliku (co daje efekt podwójnego spacjowania). Sprawdź, czy skrypt otrzymał potrzebny argument wiersza polecenia (nazwę pliku) i czy podany plik istnieje.
2. Napisz skrypt, który wyprowadza na stdout sam siebie, ale wspak.
3. Napisz skrypt archiwizujący jako plik yyyy-mm-dd-HH.MM.tar.gz wszystkie pliki w twoim katalogu domowym (/home/username), które uległy zmianie w przeciągu ostatnich 24 godzin (wykorzystaj polecenie find).
4. Napisz funkcję powłoki bash, która sprawdza, czy podany argument jest liczbą całkowitą. Funkcja ma zwracać TRUE (0), jeżeli przekazano liczbę całkowitą, i  FALSE (1) w przeciwnym razie.

Zadania średniotrudne (po 10 pkt.)
1. Napisz skrypt wyszukujący w katalogu domowym użytkownika (/home/username) wszystkie pliki o rozmiarze większym od 100 KB. Po znalezieniu pliku, daj użytkownikowi możliwość wyboru: usunięcia, kompresji lub pozostawienia pliku, po czym przejdź do następnego pliku. Zapisz do pliku dziennika wszystkie usunięte pliki wraz z czasem ich usunięcia.
2. [x] Napisz skrypt rozwiązujący równanie kwadratowe postaci a x2 + b x + c = 0. Współczynniki a, b i c przekaż jako argumenty lub poproś o nie użytkownika. Wynik podaj z dokładnością do 3 miejsc po przecinku (skorzystaj z polecenia bc).
3. Napisz funkcję powłoki bash obliczającą największy wspólny dzielnik dwóch liczb naturalnych, gcd(a, b), za pomocą algorytmu Euklidesa. Wykorzystaj tę funkcję w skrypcie obliczającym dla dwóch liczb, podanych jako argumenty wiersza polecenia, ich największy wspólny dzielnik oraz opcjonalnie ich najmniejszą wspólną wielokrotność, lcm(a, b). Wskazówka: Zachodzi lcm(a, b) = (a · b) / gcd(a, b).
4. Napisz skrypt upraszczający dodawanie użytkownika do systemu. Skrypt powinien:
- Przyjmować tylko jeden argument (nazwę użytkownika), w przeciwnym razie drukować komunikat o sposobie użycia i kończyć.
- Sprawdzić plik /etc/passwd i odnaleźć pierwszy wolny identyfikator użytkownika (UID) oraz wydrukować komunikat zawierający ten identyfikator.
- Utworzyć grupę prywatną dla użytkownika, sprawdzając plik /etc/group oraz wydrukować komunikat zawierający identyfikator grupy.
- Zebrać informację o użytkowniku: jego imię i nazwisko (opis), wybór powłoki z listy dostępnych, datę ważności konta, dodatkowe grupy, których nowy użytkownik ma być członkiem.
- Na podstawie zebranych informacji dodać odpowiednie wiersze do plików /etc/passwd, /etc/shadow, utworzyć katalog domowy użytkownika (z właściwymi prawami dostępu) oraz dodać użytkownika do wymaganych grup dodatkowych.
- Ustawić hasło użytkownika na znany domyślny łańcuch i wymusić jego zmianę przy pierwszym zalogowaniu.

Zadania trudne (po 15 pkt.)
1. [x] Napisz skrypt tworzący alfabetyczną listę słów występujących w pliku tekstowym będącym argumentem skryptu, wraz z listą wierszy, w których dane słowo wystąpiło.
2. Napisz skrypt zapisujący do dziennika wszystkie operacje dostępu do plików z katalogu /etc w ciągu danego dnia. Wpis w dzienniku powinien zawierać: nazwę pliku, nazwę użytkownika i czas dostępu oraz informację, czy plik został zmieniony. Dane zapisz jako wiersze z polami rozdzielonymi przez tabulatory.
3. [x] Napisz skrypt usuwający wszystkie komentarze ze skryptu powłoki podanego jako argument wiersza polecenia.
4. Napisz skrypt generujący łamigłówkę słowną, ukrywającą 10 podanych słów w tablicy przypadkowych liter o rozmiarach 10 × 10 (możesz wykonać to zadanie w języku angielskim).
