#!/bin/bash

function licz_delte()
{
    pow=$(bc <<< "scale=3; $2*$2")
    delta=$(bc <<< "scale=3; $pow-4*$1*$3")
}

a=$1
b=$2
c=$3

if [[ -z ${a} ]]
then
    echo "podaj a: "
    read a
fi

if [[ -z ${b} ]]
then
    echo "podaj b: "
    read b
fi

if [[ -z ${c} ]]
then
    echo "podaj c: "
    read c
fi

licz_delte ${a} ${b} ${c}

if [[ ${delta} -eq 0 ]]
then
    x=$(bc <<< "scale=3; $((-$b/$((2*$a))))")
    echo "x = ${x}"
fi

if [[ ${delta} -gt 0 ]]
then
    delta_sqrt=$(bc <<< "scale=3; sqrt(($delta))")
    mianownik=$(bc <<< "scale=3; 2*$a")
    licznik_x1=$(bc <<< "scale=3; $((-$b))-$delta_sqrt")
    licznik_x2=$(bc <<< "scale=3; $((-$b))+$delta_sqrt")
    x1=$(bc <<< "scale=3; $licznik_x1/$mianownik")
    x2=$(bc <<< "scale=3; $licznik_x2/$mianownik")
    echo "x1 = ${x1}"
    echo "x2 = ${x2}"
fi

if [[ ${delta} -lt 0 ]]
then
    echo "brak rozwiazan"
fi