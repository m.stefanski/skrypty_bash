#!/bin/bash

arr=()
i=0
comment_block=false

while read line
do
    if [[ ${i} != 0 ]]
    then
        if [[ ${line:0:1} == "#" ]]
        then
            continue
        fi
        if [[ ${line:0:2} == "<<" ]]
        then
            comment_block=true
            comment=${line:2}
            continue
        fi
        if [[ ${comment_block} == true ]]
        then
            if [[ ${line} == ${comment} ]]
            then
                comment_block=false
                comment=
                continue
            else
                continue
            fi
        fi
        arr+=("${line}")
    else
        arr+=("${line}")
    fi
    i=${i}+1
done < $1

>$1

for i in "${arr[@]}"
do
	echo ${i} >> $1
done
